## Installation

more: [Official Guide for Foliant](https://foliant-docs.github.io/docs/installation/)

### Linux

1. Установить Python3

`sudo apt update && apt install -y python3 python3-pip`

2. Установить foliant через pip

`sudo python3 -m pip install foliant foliantcontrib.init`

3. Установить pandoc

`sudo apt update && apt install -y wget texlive-full librsvg2-bin`

`sudo wget https://github.com/jgm/pandoc/releases/download/2.0.5/pandoc-2.0.5-1-amd64.deb && dpkg -i pandoc-2.0.5-1-amd64.deb`

4. Установить wkhtmltopdf

`sudo wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.focal_amd64.deb`

`sudo apt install ./wkhtmltox_0.12.6-1.focal_amd64.deb`

5. выполнить в корневой папке:

`foliant make pdf`